package com.beagile.sexystores.features.storelist;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.beagile.sexystores.R;
import com.beagile.sexystores.models.Store;
import com.beagile.sexystores.models.Store$Table;
import com.beagile.sexystores.utils.CursorUtil;
import com.beagile.sexystores.utils.UIUtil;

/**
 * Created by josh on 9/15/15.
 * <p/>
 * Converts a row from a result in the database to a store list item.
 */
public class StoreAdapter extends CursorAdapter {

    private LayoutInflater _layoutInflater;

    public StoreAdapter(Context context, Cursor c) {
        super(context, c, FLAG_REGISTER_CONTENT_OBSERVER);
        _layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = _layoutInflater.inflate(R.layout.cell_store, parent, false);

        StoreHolder holder = new StoreHolder(view);

        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        StoreHolder holder = (StoreHolder) view.getTag();
        holder.bind(cursor);
    }

    private static class StoreHolder {

        ImageView _storeImageView;
        TextView _storeNameTextView;
        TextView _storeStreetAddressTextView;
        TextView _storeCityStateZipTextView;
        TextView _storePhoneNumberTextView;

        public StoreHolder(View view) {
            _storeImageView = (ImageView) view.findViewById(R.id.store_imageview);
            _storeNameTextView = (TextView) view.findViewById(R.id.store_name_textview);
            _storeStreetAddressTextView = (TextView) view.findViewById(R.id.store_street_address_textview);
            _storeCityStateZipTextView = (TextView) view.findViewById(R.id.store_city_state_zip_textview);
            _storePhoneNumberTextView = (TextView) view.findViewById(R.id.store_phone_textview);
        }

        public void bind(Cursor cursor) {
            String imageName = CursorUtil.getString(cursor, Store$Table.STORE_LOGO_URL);
            UIUtil.loadImage(_storeImageView.getContext(), imageName, _storeImageView, R.drawable.ic_store_black_48dp);

            _storeNameTextView.setText(CursorUtil.getString(cursor, Store$Table.STORE_NAME));
            _storeStreetAddressTextView.setText(CursorUtil.getString(cursor, Store$Table.STREET_ADDRESS));
            _storeCityStateZipTextView.setText(getCityStateZip(cursor));
            _storePhoneNumberTextView.setText(CursorUtil.getString(cursor, Store$Table.PHONE_NUMBER));
        }

        private String getCityStateZip(Cursor cursor) {
            String city = CursorUtil.getString(cursor, Store$Table.CITY);
            String state = CursorUtil.getString(cursor, Store$Table.STATE);
            String zip = CursorUtil.getString(cursor, Store$Table.ZIP_CODE);

            return Store.getCityStateZip(city, state, zip);
        }
    }
}
