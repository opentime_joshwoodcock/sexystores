package com.beagile.sexystores.models;

import android.text.TextUtils;

import com.beagile.sexystores.config.SexyStoresDatabase;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by josh on 9/15/15.
 * <p/>
 * A store is where you can go to buy things.
 */
@Table(databaseName = SexyStoresDatabase.NAME)
public class Store extends BaseModel implements Serializable {

    // region: DBFlow properties

    @Column(name = "_id", getterName = "getAutoIncrementID", setterName = "setAutoIncrementID")
    @PrimaryKey(autoincrement = true)
    private int _autoIncrementID;

    @SerializedName("storeID")
    @Column(name = "store_id", getterName = "getStoreID", setterName = "setStoreID")
    private int mStoreID;

    @SerializedName("name")
    @Column(name = "store_name", getterName = "getStoreName", setterName = "setStoreName")
    private String mStoreName;

    @SerializedName("address")
    @Column(name = "street_address", getterName = "getStreetAddress", setterName = "setStreetAddress")
    private String mStreetAddress;

    @SerializedName("city")
    @Column(name = "city", getterName = "getCity", setterName = "setCity")
    private String mCity;

    @SerializedName("state")
    @Column(name = "state", getterName = "getState", setterName = "setState")
    private String mState;

    @SerializedName("zipcode")
    @Column(name = "zip_code", getterName = "getZipCode", setterName = "setZipCode")
    private String mZipCode;

    @SerializedName("latitude")
    @Column(name = "latitude", getterName = "getLatitude", setterName = "setLatitude")
    private double mLatitude;

    @SerializedName("longitude")
    @Column(name = "longitude", getterName = "getLongitude", setterName = "setLongitude")
    private double mLongitude;

    @SerializedName("storeLogoURL")
    @Column(name = "store_logo_url", getterName = "getStoreLogoURL", setterName = "setStoreLogoURL")
    private String mStoreLogoURL;

    @SerializedName("phone")
    @Column(name = "phone_number", getterName = "getPhoneNumber", setterName = "setPhoneNumber")
    private String mPhoneNumber;

    // endregion

    public Store() {
    }

    // region: Getters

    public static String getCityStateZip(String city, String state, String zipCode) {
        List<String> pieces = new ArrayList<>();

        if (TextUtils.isEmpty(city)) {
            pieces.add(city);
        }

        if (!TextUtils.isEmpty(state)) {
            pieces.add(state);
        }

        if (TextUtils.isEmpty(zipCode)) {
            pieces.add(zipCode);
        }

        return TextUtils.join(" ", pieces);
    }

    public int getAutoIncrementID() {
        return _autoIncrementID;
    }

    public void setAutoIncrementID(int _autoIncrementID) {
        this._autoIncrementID = _autoIncrementID;
    }

    public String getStoreName() {
        return mStoreName;
    }

    public void setStoreName(String mStoreName) {
        this.mStoreName = mStoreName;
    }

    public String getStreetAddress() {
        return mStreetAddress;
    }

    public void setStreetAddress(String mStreetAddress) {
        this.mStreetAddress = mStreetAddress;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String mCity) {
        this.mCity = mCity;
    }

    public String getState() {
        return mState;
    }

    public void setState(String mState) {
        this.mState = mState;
    }

    public String getZipCode() {
        return mZipCode;
    }

    // endregion

    // region: Setters

    public void setZipCode(String mZipCode) {
        this.mZipCode = mZipCode;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getStoreLogoURL() {
        return mStoreLogoURL;
    }

    public void setStoreLogoURL(String mStoreLogoURL) {
        this.mStoreLogoURL = mStoreLogoURL;
    }

    public int getStoreID() {
        return mStoreID;
    }

    public void setStoreID(int mStoreID) {
        this.mStoreID = mStoreID;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }

    // endregion
}
