package com.beagile.sexystores.config;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by josh on 9/15/15.
 * <p/>
 * Configurations for database.
 */
@Database(name = SexyStoresDatabase.NAME, version = SexyStoresDatabase.VERSION)
public class SexyStoresDatabase {

    public static final String NAME = "SEXY_STORES";
    public static final int VERSION = 1;
}
