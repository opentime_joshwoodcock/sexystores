package com.beagile.sexystores.application;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

/**
 * Created by josh on 9/15/15.
 * <p/>
 * Custom application for SexyStores
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
    }
}
