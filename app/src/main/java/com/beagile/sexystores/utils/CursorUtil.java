package com.beagile.sexystores.utils;

import android.database.Cursor;
import android.support.annotation.NonNull;

/**
 * Created by Josh Woodcock on 27-08-2015.
 * <p/>
 * Simplifies the extraction of data from a cursor.
 */
public class CursorUtil {

    private CursorUtil() {
    }

    public static int getInt(@NonNull Cursor cursor, @NonNull String columnName) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName));
    }

    public static Long getLong(@NonNull Cursor cursor, @NonNull String columnName) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(columnName));
    }

    public static boolean getBoolean(@NonNull Cursor cursor, @NonNull String columnName) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName)) == 1;
    }

    public static String getString(@NonNull Cursor cursor, @NonNull String columnName) {
        String result = cursor.getString(cursor.getColumnIndexOrThrow(columnName));
        return result != null ? result : "";
    }
}
