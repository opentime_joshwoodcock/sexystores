package com.beagile.sexystores.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * Created by josh on 9/15/15.
 * <p/>
 * Simplifies operations that deal with the user interface.
 */
public class UIUtil {

    private UIUtil() {
    }

    public static void loadImage(Context ctx, String imageName, ImageView imageView, int placeHolder) {
        RequestCreator requestCreator;

        requestCreator = Picasso.with(ctx)
                .load(imageName)
                .placeholder(ContextCompat.getDrawable(ctx, placeHolder))
                .error(ContextCompat.getDrawable(ctx, placeHolder));

        requestCreator.into(imageView);
    }
}
