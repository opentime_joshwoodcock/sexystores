# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/josh/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#opentime
-keep class com.opentimeapp.opentime.animations.** { *; }

#android
-keep class com.google.android.gms.ads.identifier.** { *; }

#pushtorefresh
-dontwarn com.pushtorefresh.*
-dontwarn com.pushtorefresh.javac_warning_annotation.internal.WarningAnnotationProcessor

#okio
-dontwarn okio.**

#retrofit
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**
-dontwarn rx.**
-dontwarn retrofit.**
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

#gson
-keepattributes Signature
-keep class com.google.gson.** { *; }

#Custom views inflated from XML
-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

# will keep line numbers and file name obfuscation
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

# hockeyapp
-keep public class javax.net.ssl.**
-keepclassmembers public class javax.net.ssl.** { *; }
-keep public class org.apache.http.**
-keepclassmembers public class org.apache.http.** { *; }
-keepclassmembers class net.hockeyapp.android.UpdateFragment { *; }

# saripaar
-keep class com.mobsandgeeks.saripaar.** {*;}
-keep @com.mobsandgeeks.saripaar.annotation.ValidateUsing class * {*;}

